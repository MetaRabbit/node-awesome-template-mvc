const express = require('express')
const namespace = require('require-namespace')
const fp = require('path')

// templates
const hbs = require('express-hbs')

// session and security
const flash = require('connect-flash')
const passport = require('passport')
const session = require('express-session')
// connect for redis store: const RedisStore = require('connect-redis')(session)

// advance security service
const helmet = require('helmet')

// parsers
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')

// developments service
// http logger
const morgan = require('morgan')

// production logging and monitoring
var log4js = require('log4js')
log4js.configure('./config/log4js.json')
var log = log4js.getLogger('app')

log.debug('test')

namespace.createSync(__dirname + '/src/application', 'application')
namespace.createSync(__dirname + '/src/infrastructure', 'infrastructure')
namespace.createSync(__dirname + '/src/utils', 'utils')

let application = namespace.application
let infrastructure = namespace.infrastructure
var utils = namespace.utils

// init db connection
infrastructure.psqlConnection.init()

// app.use(helmet());
var app = express()

app.use(log4js.connectLogger(log4js.getLogger('http'), { level: 'auto' }))
app.set('json spaces', 40)

initViewEngine()
initStaticFiles()
initDevService()
initParsers()
initSessionSecurity()
initMvc()

function initMvc () {
  app.use('/', application.homeController)
  app.use('/login', application.loginController)
  app.use('/service', application.serviceController)

  app.use(utils.webErrorHandler)
}

function initSessionSecurity () {
  application.authInit()

  // Use for redis session store
  /* var redisClient = new RedisStore({
        host: 'localhost',
        port: 6379
    }) */

  var sessionMiddleware = session({
    // store: redisClient,
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: {
      maxAge: 9999999,
      secure: false
    }
  })
  app.use(sessionMiddleware)

  app.use(flash())
  app.use(passport.initialize())
  app.use(passport.session())
}

function initParsers () {
  app.use(bodyParser.urlencoded({
    extended: false
  }))
  app.use(bodyParser.json())

  app.use(cookieParser()) // read cookies (needed for auth)
}

function initDevService () {
  app.use(morgan('dev')) // log every request to the console
}

function initStaticFiles () {
  app.use(express.static('public'))
}

function initViewEngine () {
  var helpers = require('handlebars-helpers')({
    handlebars: hbs
  })
  hbs.registerHelper('json', function (context) {
    return JSON.stringify(context)
  })

  app.engine('hbs', hbs.express4({
    partialsDir: fp.join(__dirname, '/views/partials'),
    defaultLayout: fp.join(__dirname, '/views/layout/mainLayout.hbs'),
    layoutsDir: fp.join(__dirname, 'views/layout')
  }))

  app.set('view engine', 'hbs')
  app.set('views', __dirname + '/views')
}

module.exports = app
