const psqlConnection = require('./psqlConnection')
var validate = require('aproba')
const namespace = require('require-namespace')
let utils = namespace.utils

var errorHandler = utils.errorHandler
module.exports = class RepositoryPsql {
  constructor (tableName, namePrimaryKey = '') {
    this.conn = psqlConnection
    this._tableName = tableName
    if (namePrimaryKey === '') {
      namePrimaryKey = this._tableName + '_id'
    }
    this._namePrimaryKey = namePrimaryKey
  }

  async findOne (criterion) {
    validate('O', arguments)

    var filter = this._buildQueryFilter(criterion)
    var query = 'SELECT * FROM ' + this._tableName + ' WHERE ' + filter
    return this.conn.get().oneOrNone(query, criterion)
      .then((data) => {
        return data
      })
      .catch(async error => {
        errorHandler(error)
        return { status: 'failed', err: error }
      })
  }

  async findMany (criterion) {
    validate('O', arguments)

    var filter = this._buildQueryFilter(criterion)
    var query = 'SELECT * FROM ' + this._tableName + ' WHERE ' + filter
    return this.conn.get().manyOrNone(query, criterion)
      .then((data) => {
        return data
      })
      .catch(async error => {
        errorHandler(error)
        return { status: 'failed', err: error }
      })
  }

  _buildQueryFilter (criterion) {
    var filters = []

    for (var key in criterion) {
      if (Object.prototype.hasOwnProperty.call(criterion, key)) {
        filters.push(key.toString() + '=' + '$/' + key.toString() + '/')
      }
    }

    var filter = filters.join(' AND ')

    return filter
  }
}
