
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const userRepository = require('../service/repository/userRepository')
const claimRepository = require('../service/repository/claimRepository')
const namespace = require('require-namespace')
let utils = namespace.utils

const authMiddleware = require('./authMiddleware')

async function findUser (username, callback) {
  var account = await userRepository.getUserByName(username)
  if (account != null) {
    let user = {}
    user.username = account.username
    user.password = account.password_hash
    user.email = account.email
    user.password_salt = account.password_salt
    user.id = account.account_id
    let claims = await claimRepository.getClaimsString(account.account_id)
    user.claims = claims
    return callback(null, user)
  }

  return callback(null)
}

passport.serializeUser(async function (user, cb) {
  cb(null, { id: user.id, email: user.email, perms: await user.claims, userId: user.id, username: user.username })
})

passport.deserializeUser(function (user, cb) {
  findUser(user.username, cb)
})

function initPassport () {
  passport.use(
    new LocalStrategy(

      function (username, password, done) {
        findUser(username, function (err, user) {
          if (err) {
            return done(err)
          }
          if (!user) {
            return done(null, false, { message: 'account not found' })
          }
          var saltPassword = utils.passHashing.sha512(password, user.password_salt)
          if (saltPassword.passwordHash !== user.password) {
            return done(null, false, { message: 'password or login failed' })
          }
          return done(null, user)
        })
      }
    ))

  passport.authenticationMiddleware = authMiddleware
}

module.exports = initPassport
