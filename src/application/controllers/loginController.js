var express = require('express')
var router = express.Router()
const passport = require('passport')

const authMiddleware = require('../authentication/authMiddleware')

router.get('/', function (req, res) {
  // if(req.session) res.redirect('/profile')
  res.render('loginView', { message: req.flash('error'), layout: 'mainLayout' })
})

router.post('/', passport.authenticate('local', {
  failureRedirect: '/login',
  failureFlash: true
}), async function (req, res) {
  if (req.body.remember === '') {
    req.session.cookie.maxAge = 30 * 24 * 60 * 60 * 1000 // Cookie expires after 30 days
  } else {
    req.session.cookie.expires = false // Cookie expires at end of session
  }

  res.redirect('/')
})

router.get('/logout', authMiddleware(), async function (req, res, next) {
  req.logout()
  res.redirect('/')
})

module.exports = router
