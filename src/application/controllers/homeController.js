var express = require('express')
var router = express.Router()

router.get('/', function (req, res) {
  if (req.isAuthenticated()) {
    let username = req.session.passport.user.username
    res.render('homeView', { isAuth: true, username })
  } else {
    res.render('homeView', { isAuth: false })
  }
})

module.exports = router
