function middleware () {
  return function (req, res, next) {
    if (req.session.passport.user.perms.includes(req.params.type + ':' + req.query.id)) {
      next()
    } else {
      res.write('Access Denied')
      res.end()
    }
  }
}

module.exports = middleware
