const namespace = require('require-namespace')
const RepositoryPsql = namespace.infrastructure.repositoryPsql
const Claim = require('../../authorization/claim')

class ClaimRepository extends RepositoryPsql {
  constructor () {
    super('claim')
  }

  async getClaimsString (userId) {
    var claims = await super.findMany({ user_id: userId })
    if (!claims) return ''
    claims = convertDomainToDtoArrayClaims(claims)
    claims = claims.map(claim => claim.toString())
    return claims.join('&')
  }

  async getTypedClaims (profileId, typeResource) {
    var claims = await super.findMany({ profile_id: profileId, type_resource_name: typeResource })
    if (!claims) return []
    return convertDomainToDtoArrayClaims(claims)
  }
}

function convertDomainToDtoArrayClaims (claims) {
  return claims.map(claim => {
    return new Claim(claim.claim_id, claim.profile_id, claim.type_resource_name, claim.value)
  })
}

module.exports = new ClaimRepository()
