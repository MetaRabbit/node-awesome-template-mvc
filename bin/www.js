var app = require('../app')
var http = require('http')
const normalizePort = require('normalize-port')

/**
 * Initialise log4js first, so we don't miss any log messages
 */
var log4js = require('log4js')
log4js.configure('./config/log4js.json')

var log = log4js.getLogger('startup')
/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '3000')
app.set('port', port)

/**
 * Create HTTP server.
 */

var server = app.listen(app.get('port'), function() {
  log.info('Express server listening on port ', server.address().port, " with pid ", process.pid )
  console.log('Express server listening on port ', server.address().port, " with pid ", process.pid )
})